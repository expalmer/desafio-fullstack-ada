# Desafio Técnico Fullstack Ada

1. Clonar o projeto

```console
git clone https://gitlab.com/expalmer/desafio-fullstack-ada.git
```

2. Levantando o Server

```console
> cd back
> nvm use (ou mude a versão do node para a que está no .nvmrc)
> cp .env-example .env
> yarn initialize
```

O `yarn initialize` vai instalar as dependencias, configurar o Prisma com o banco sqlite, dar build e subit na porta 9000.

Servidor rodando!

3. Levantando o Client

```console
> cd front
> nvm use (ou mude a versão do node para a que está no .nvmrc)
> yarn && yarn dev
```

Pronto, já deve estar rodando -> [http://localhost:5173/](http://localhost:5173/)

## Features

- Incluir, alterar e remover card
- Alterar o card de section
- Alterar a ordem do card na section
- Edição em markdown com highlight de código
- Login automático. (Não faça isso em casa crianças)

## Nive to Have

- Não implementei quando o token expira
- Design mais arrojado
