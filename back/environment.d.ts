export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      JWT_SECRET: string;
      DEFAULT_LOGIN: string;
      DEFAULT_PASSWORD: string;
      PORT: string;
    }
  }
}
