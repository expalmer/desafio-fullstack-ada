import { AnyZodObject, ZodError } from "zod";
import { AuthError, DefaultError, NotFoundError } from "../errors.js";
import { NextFunction, Request, Response } from "express";

import { Service } from "../service/index.js";
import { validateToken } from "../utils/auth.js";

export const auth = async (req: Request, _res, next: NextFunction) => {
  const { authorization = "" } = req.headers;

  if (!authorization) {
    next(new AuthError("Token não informado"));
  }

  const [, token] = authorization.split(" ");

  const payload = await validateToken(token);

  if (!payload) {
    next(new AuthError("Token inválido"));
  }

  req.body.payload = payload;

  return next();
};

export const validate =
  (schema: AnyZodObject) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      await schema.parseAsync({
        body: req.body,
        query: req.query,
        params: req.params,
      });
      return next();
    } catch (error) {
      next(error);
    }
  };

export const guardForCardId =
  (service: Service) => async (req: Request, _res, next: NextFunction) => {
    const cardId = req?.params?.id || 0;

    const card = await service.getOne(+cardId);
    if (!card) {
      return next(new NotFoundError("Card não encontrado"));
    }
    const { titulo } = req.body;
    const dateTime = new Date().toLocaleString("pt-br");

    switch (req.method) {
      case "PUT":
        console.info(
          "\x1b[34m",
          `${dateTime} - Card ${cardId} - ${titulo} - Alterar`,
          "\x1b[0m"
        );
        break;

      case "DELETE":
        console.info(
          "\x1b[31m",
          `${dateTime} - Card ${cardId} - ${card.titulo} - Remover`,
          "\x1b[0m"
        );

        break;
    }

    next();
  };

export const errorHandler = (err, _req, res: Response, next: NextFunction) => {
  if (err) {
    if (err instanceof ZodError) {
      return res.status(400).json({
        message: err.name,
        errors: err.errors,
      });
    }
    if (err instanceof DefaultError) {
      return res.status(err.code).json({
        message: err.message,
      });
    }
    console.log(err);

    return res.status(500).json({
      message: "Erro interno",
    });
  }

  return next();
};
