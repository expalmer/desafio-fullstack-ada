import { PrismaClient } from "@prisma/client";
import { createContainer } from "./container.js";
import { createServer } from "./server.js";
import dotenv from "dotenv";

dotenv.config();

const { PORT } = process.env;
async function init() {
  try {
    const client = new PrismaClient();
    const container = createContainer(client);
    const app = createServer(container);

    app.listen(Number(PORT), () => {
      console.log(`SERVER Started: on http://localhost:${PORT}`);
    });
  } catch (error) {
    console.log("OPS", error);
  }
}

init();
