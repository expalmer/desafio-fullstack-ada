import {
  CardBody,
  IdParam,
  createCardSchema,
  deleteCardSchema,
  moveListSchema,
  moveOrderSchema,
  updateCardSchema,
  userSchema,
} from "./schemas.js";
import { Container, LoginBody } from "./types.js";
import {
  auth,
  errorHandler,
  guardForCardId,
  validate,
} from "./middlewares/index.js";
import express, { Request } from "express";

import bodyParser from "body-parser";
import cors from "cors";

export const createServer = ({ service }: Container) => {
  const app = express();

  app.use(bodyParser.json());
  app.use(cors());

  app.post(
    "/login",
    validate(userSchema),
    async (req: Request<{}, {}, LoginBody>, res, next) => {
      try {
        const token = await service.login(req.body);

        res.json({
          token,
        });
      } catch (error) {
        next(error);
      }
    }
  );

  app.get("/cards", auth, async (req, res, next) => {
    try {
      const cards = await service.getAll();
      res.json(cards);
    } catch (error) {
      next(error);
    }
  });

  app.post(
    "/cards",
    auth,
    validate(createCardSchema),
    async (req: Request<{}, {}, CardBody>, res, next) => {
      try {
        const card = await service.store(req.body);

        res.json(card);
      } catch (error) {
        next(error);
      }
    }
  );

  app.put(
    "/cards/:id",
    auth,
    validate(updateCardSchema),
    guardForCardId(service),
    async (req: Request<IdParam, {}, CardBody>, res, next) => {
      try {
        const { id } = req.params;
        const card = await service.update(Number(id), req.body);

        res.json(card);
      } catch (error) {
        next(error);
      }
    }
  );

  app.delete(
    "/cards/:id",
    auth,
    validate(deleteCardSchema),
    guardForCardId(service),
    async (req: Request<IdParam, {}, {}>, res, next) => {
      try {
        const { id } = req.params;
        const card = await service.delete(Number(id));

        res.json(card);
      } catch (error) {
        next(error);
      }
    }
  );

  app.patch(
    "/cards/:id/move-list",
    auth,
    validate(moveListSchema),
    guardForCardId(service),
    async (req: Request<IdParam, {}, { lista: string }>, res, next) => {
      try {
        const { id } = req.params;
        const { lista } = req.body;
        const card = await service.moveList(Number(id), lista);

        res.json(card);
      } catch (error) {
        next(error);
      }
    }
  );

  app.patch(
    "/cards/:id/move-order",
    auth,
    validate(moveOrderSchema),
    guardForCardId(service),
    async (req: Request<IdParam, {}, { up: boolean }>, res, next) => {
      try {
        const { id } = req.params;
        const { up = false } = req.body;
        const card = await service.moveOrder(Number(id), up);

        res.json(card);
      } catch (error) {
        next(error);
      }
    }
  );

  app.use((_req, res) => {
    res.status(404).json({ message: "Page Not Found" });
  });

  app.use(errorHandler);

  return app;
};
