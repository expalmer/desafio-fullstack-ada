import { z } from "zod";

export const userSchema = z.object({
  body: z.object({
    login: z.string({
      required_error: "Login é obrigatório",
    }),
    senha: z.string({
      required_error: "Senha é obrigatória",
    }),
  }),
});

export const idSchema = z.object({
  id: z.string({
    required_error: "ID é obrigatório",
  }),
});

export type IdParam = z.infer<typeof idSchema>;

export const cardSchema = z.object({
  titulo: z.string({
    required_error: "Título é obrigatório",
  }),
  conteudo: z.string({
    required_error: "Conteudo é obrigatório",
  }),
  ordem: z.number({
    required_error: "Ordem é obrigatória",
  }),
  lista: z.string({
    required_error: "Lista é obrigatória",
  }),
});

export type CardBody = z.infer<typeof cardSchema>;

export const createCardSchema = z.object({
  body: cardSchema,
});

export const updateCardSchema = z.object({
  params: idSchema,
  body: cardSchema,
});

export const deleteCardSchema = z.object({
  params: idSchema,
});

export const moveListSchema = z.object({
  params: idSchema,
  body: z.object({
    lista: z.string(),
  }),
});

export const moveOrderSchema = z.object({
  params: idSchema,
  body: z.object({
    up: z.boolean(),
  }),
});
