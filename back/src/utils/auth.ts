import { SignJWT, jwtVerify } from "jose";

const { JWT_SECRET } = process.env;
export const getToken = async (login: string) => {
  const jwt = await new SignJWT({
    usu: login,
  })
    .setProtectedHeader({ alg: "HS256" })
    .setIssuedAt()
    .setExpirationTime("1h")
    .sign(new TextEncoder().encode(JWT_SECRET));

  return jwt;
};

export const validateToken = async (token: string) => {
  try {
    const { payload } = await jwtVerify(
      token,
      new TextEncoder().encode(JWT_SECRET)
    );
    return payload;
  } catch (error) {
    return false;
  }
};
