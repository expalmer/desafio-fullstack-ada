import { CardType, List } from "../types.js";

import { Cards } from "@prisma/client";

export const parseCard = (row: Cards): CardType => ({
  id: +row.id,
  titulo: row.title,
  conteudo: row.content,
  lista: row.list as List,
  ordem: row.order,
  criado_em: row.createdAt.toISOString(),
});
