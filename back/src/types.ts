import { Service } from "./service/index.js";

export type List = "todo" | "doing" | "done";

export type LoginBody = {
  login: string;
  senha: string;
};

export type Container = {
  service: Service;
};

export type CardType = {
  id: number;
  titulo: string;
  conteudo: string;
  lista: List;
  ordem: number;
  criado_em: string;
};
