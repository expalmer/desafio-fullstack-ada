import { Container } from "./types.js";
import { PrismaClient } from "@prisma/client";
import { Repository } from "./repository/index.js";
import { Service } from "./service/index.js";

export const createContainer = (client: PrismaClient): Container => {
  const repository = new Repository(client);
  const service = new Service(repository);

  return {
    service,
  };
};
