import { Prisma, PrismaClient } from "@prisma/client";

export class Repository {
  client: PrismaClient;

  constructor(client: PrismaClient) {
    this.client = client;
  }

  async findAll() {
    const cards = await this.client.cards.findMany({
      orderBy: {
        order: "asc",
      },
    });

    return cards;
  }

  async findById(id: number) {
    const card = this.client.cards.findUnique({
      where: {
        id,
      },
    });

    return card;
  }

  async findLastOrderByList(list: string) {
    const card = await this.client.cards.findFirst({
      orderBy: {
        order: "desc",
      },
      where: {
        list,
      },
    });

    return card?.order || 0;
  }

  async create(data: Prisma.CardsCreateInput) {
    const card = await this.client.cards.create({
      data: {
        ...data,
        order: (await this.findLastOrderByList(data.list)) + 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    });

    return card;
  }

  async udpate(id: number, data: Prisma.CardsUpdateInput) {
    const card = await this.client.cards.update({
      where: {
        id,
      },
      data: {
        ...data,
        updatedAt: new Date(),
      },
    });

    return card;
  }

  async delete(id: number) {
    const card = await this.client.cards.delete({
      where: {
        id,
      },
    });

    return card;
  }
}
