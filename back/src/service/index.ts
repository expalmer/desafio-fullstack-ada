import { AuthError } from "../errors.js";
import { CardBody } from "../schemas.js";
import { LoginBody } from "../types.js";
import { Prisma } from "@prisma/client";
import { Repository } from "../repository/index.js";
import { getToken } from "../utils/auth.js";
import { parseCard } from "../utils/parse-card.js";

const { DEFAULT_LOGIN, DEFAULT_PASSWORD } = process.env;

export class Service {
  repository: Repository;

  constructor(repository: Repository) {
    this.repository = repository;
  }

  async login({ login, senha }: LoginBody): Promise<string> {
    if (login !== DEFAULT_LOGIN || senha !== DEFAULT_PASSWORD) {
      throw new AuthError("Login ou senha inválidos");
    }

    const token = await getToken(login);
    return token;
  }

  async getAll() {
    const cards = await this.repository.findAll();
    return cards.map(parseCard);
  }

  async getOne(id: number) {
    const card = await this.repository.findById(id);
    return parseCard(card);
  }

  async store({ titulo, conteudo, lista, ordem }: CardBody) {
    const item: Prisma.CardsCreateInput = {
      list: lista,
      title: titulo,
      content: conteudo,
      order: ordem,
    };

    const card = await this.repository.create(item);

    return parseCard(card);
  }

  async update(id: number, { titulo, conteudo, lista, ordem }: CardBody) {
    const item: Prisma.CardsUpdateInput = {
      list: lista,
      title: titulo,
      content: conteudo,
      order: ordem,
    };

    const card = await this.repository.udpate(id, item);

    return parseCard(card);
  }

  async delete(id: number) {
    await this.repository.delete(id);

    return this.getAll();
  }

  async moveList(id: number, lista: string) {
    const lastOrder = await this.repository.findLastOrderByList(lista);
    const item: Prisma.CardsUpdateInput = {
      list: lista,
      order: lastOrder + 1,
    };

    const updatedCard = await this.repository.udpate(id, item);

    return parseCard(updatedCard);
  }

  async moveOrder(id: number, up: boolean) {
    const card = await this.repository.findById(id);
    const cards = await this.repository.findAll();
    const cardsInList = cards.filter((c) => c.list === card.list);

    const index = cardsInList.findIndex((c) => c.id === id);
    const index2 = up ? index - 1 : index + 1;
    const card2 = cardsInList[index2];
    if (!card2) {
      return;
    }
    const item: Prisma.CardsUpdateInput = {
      order: card2.order,
    };

    await this.repository.udpate(id, item);

    const item2: Prisma.CardsUpdateInput = {
      order: card.order,
    };

    await this.repository.udpate(card2.id, item2);

    return;
  }
}
