export type List = "todo" | "doing" | "done";

export type CardType = {
  id: number;
  titulo: string;
  conteudo: string;
  lista: string;
  ordem: number;
  criado_em: string;
};

export type NewCardType = Omit<CardType, "criado_em" | "id">;

export type MoveListType = {
  id: number;
  lista: string;
};

export type MoveOrderType = {
  id: number;
  up: boolean;
};
