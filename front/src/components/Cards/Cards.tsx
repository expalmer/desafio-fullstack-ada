import * as styles from "./Cards.styles";

type CardsProps = {
  children: React.ReactNode;
};

export const Cards = ({ children }: CardsProps) => {
  return <styles.Container>{children}</styles.Container>;
};
