import * as styles from "./ActionIcon.styles";

type ActionIconProps = React.ComponentPropsWithoutRef<"button"> & {
  children: React.ReactNode;
};

export const ActionIcon = ({ children, ...props }: ActionIconProps) => {
  return <styles.Container {...props}>{children}</styles.Container>;
};
