import styled from "styled-components";

export const Container = styled.button`
  appearance: none;
  background-color: transparent;
  border: 0;
  color: inherit;
  cursor: pointer;
  font-size: 14px;
  padding: 0;
  text-align: left;
  text-decoration: none;
  touch-action: manipulation;
  color: #fff;
  background-color: var(--purple);
  width: 32px;
  height: 32px;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;

  transition-property: border-color, background, color, transform, box-shadow;
  transition-duration: 0.15s;
  transition-timing-function: ease;

  &:focus-visible {
    outline: 1px solid blue;
    outline-offset: 1px;
  }

  &:active {
    transform: translateY(1px);
  }

  &:hover {
    background-color: #000;
    color: #fff;
  }

  &:disabled {
    background-color: #ddd;
    color: #fff;
    cursor: not-allowed;
  }
`;
