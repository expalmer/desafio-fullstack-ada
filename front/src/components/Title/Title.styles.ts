import styled from "styled-components";

export const Container = styled.h4`
  padding: 0;
  margin: 0;
  font-size: 18px;
  font-weight: 700;
  line-height: 24px;
  text-align: center;
  text-transform: uppercase;
  color: var(--purpleDark);
  gap: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Icon = styled.span`
  color: var(--purple);
  display: flex;
  align-items: center;
  justify-content: center;
`;
