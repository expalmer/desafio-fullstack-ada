import * as styles from "./Title.styles";

type TitleProps = {
  children: React.ReactNode;
  icon: React.ReactNode;
};

export const Title = ({ children, icon }: TitleProps) => {
  return (
    <styles.Container>
      <styles.Icon>{icon}</styles.Icon>
      {children}
    </styles.Container>
  );
};
