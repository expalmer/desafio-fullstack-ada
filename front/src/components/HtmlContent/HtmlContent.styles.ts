import styled from "styled-components";

export const Container = styled.div`
  p {
    margin: 0;
    padding: 0;
    font-size: 16px;
    line-height: 24px;
  }
`;
