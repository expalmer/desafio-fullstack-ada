import * as styles from "./HtmlContent.styles";

import { toView } from "../../utils";

type HtmlContentProps = {
  content: string;
};

export const HtmlContent = ({ content }: HtmlContentProps) => {
  return (
    <styles.Container
      dangerouslySetInnerHTML={{
        __html: toView(content),
      }}
    />
  );
};
