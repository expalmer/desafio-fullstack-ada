import * as styles from "./TextArea.styles";

import { InputHTMLAttributes, useEffect, useRef } from "react";

type TextAreaProps = InputHTMLAttributes<HTMLTextAreaElement>;

export const TextArea = (props: TextAreaProps) => {
  const ref = useRef<HTMLDivElement>(null);

  // const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
  //   props?.onChange?.(e);
  //   ref.current?.setAttribute("data-replicated-value", e.currentTarget.value);
  // };

  useEffect(() => {
    ref.current?.setAttribute(
      "data-replicated-value",
      String(props?.value) ?? ""
    );
  }, [props.value]);

  return (
    <styles.GrowWrap ref={ref}>
      <styles.Container {...props} />
    </styles.GrowWrap>
  );
};
