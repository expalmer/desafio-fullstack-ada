import styled from "styled-components";

export const Container = styled.textarea`
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  resize: none;

  &:focus {
    color: #495057;
    background-color: #fff;
    border-color: #80bdff;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
  }

  &::placeholder {
    opacity: 0.5;
  }
`;

export const GrowWrap = styled.div`
  display: grid;

  &::after {
    content: attr(data-replicated-value) " ";
    white-space: pre-wrap;
    visibility: hidden;
  }
  & > textarea {
    resize: none;
    overflow: hidden;
  }
  & > textarea,
  &::after {
    border: 1px solid black;
    padding: 0.5rem;
    font: inherit;
    grid-area: 1 / 1 / 2 / 2;
  }
`;
