import * as styles from "./Main.styles";

type MainProps = {
  children: React.ReactNode;
};

export const Main = ({ children }: MainProps) => {
  return (
    <styles.Container>
      <styles.Inner>{children}</styles.Inner>
    </styles.Container>
  );
};
