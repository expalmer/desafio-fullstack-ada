import styled from "styled-components";

export const Container = styled.div`
  flex-grow: 1;
  position: relative;
  padding: 40px;
`;

export const Inner = styled.div`
  display: flex;
  flex-direction: row;
  list-style: none;
  padding: 0 8px;
  gap: 16px;
`;
