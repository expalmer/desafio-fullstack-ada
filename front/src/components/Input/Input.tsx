import * as styles from "./Input.styles";

import { InputHTMLAttributes } from "react";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  isReadOnly?: boolean;
};

export const Input = ({ isReadOnly, ...props }: InputProps) => {
  return (
    <styles.Container
      {...props}
      isReadOnly={isReadOnly}
      disabled={props.disabled || isReadOnly}
    />
  );
};
