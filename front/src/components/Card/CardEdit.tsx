import * as styles from "./Card.styles";

import { IconCheck, IconForbid } from "@tabler/icons-react";
import { useCallback, useState } from "react";

import { ActionIcon } from "../ActionIcon";
import { CardType } from "../../types";
import { Input } from "../Input/Input";
import { TextArea } from "../TextArea/TextArea";

type CardEditProps = {
  card: CardType;
  onUpdate: (card: CardType) => void;
  closeEdit: () => void;
};

export const CardEdit = ({ card, onUpdate, closeEdit }: CardEditProps) => {
  const [title, setTitle] = useState(card.titulo);
  const [content, setContent] = useState(card.conteudo);

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      if (!title || !content) return;

      onUpdate({
        ...card,
        titulo: title,
        conteudo: content,
      });
      setContent("");
      setTitle("");
      closeEdit();
    },
    [card, content, onUpdate, title, closeEdit]
  );

  return (
    <styles.Container as="form" onSubmit={handleSubmit}>
      <Input
        placeholder="Titulo"
        name="titulo"
        value={title}
        onChange={(e) => setTitle(e.currentTarget.value)}
      />
      <TextArea
        placeholder="Descrição"
        name="conteudo"
        value={content}
        onChange={(e) => setContent(e.currentTarget.value)}
      />
      <styles.Actions>
        <ActionIcon>
          <IconForbid onClick={closeEdit} type="button" />
        </ActionIcon>
        <ActionIcon disabled={!title || !content}>
          <IconCheck type="submit" />
        </ActionIcon>
      </styles.Actions>
    </styles.Container>
  );
};
