import * as styles from "./Card.styles";

import { ActionIcon } from "../ActionIcon";
import { IconPlus } from "@tabler/icons-react";
import { Input } from "../Input/Input";
import { NewCardType } from "../../types";
import { TextArea } from "../TextArea/TextArea";
import { useState } from "react";

type CardProps = {
  onCreate: (newTask: NewCardType) => void;
};

export const NewCard = ({ onCreate }: CardProps) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!title || !content) return;

    onCreate({
      titulo: title,
      conteudo: content,
      ordem: 0,
      lista: "todo",
    });
    setContent("");
    setTitle("");
  };

  return (
    <styles.Container as="form" onSubmit={handleSubmit}>
      <Input
        placeholder="Titulo"
        name="titulo"
        value={title}
        onChange={(e) => setTitle(e.currentTarget.value)}
      />
      <TextArea
        placeholder="Conteúdo"
        name="conteudo"
        value={content}
        onChange={(e) => setContent(e.currentTarget.value)}
      />
      <styles.Actions>
        <ActionIcon type="submit" disabled={!title || !content}>
          <IconPlus />
        </ActionIcon>
      </styles.Actions>
    </styles.Container>
  );
};
