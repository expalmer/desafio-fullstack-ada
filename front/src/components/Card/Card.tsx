import { CardEdit } from "./CardEdit";
import { CardType } from "../../types";
import { CardView } from "./CardView";
import { useState } from "react";

type CardProps = {
  card: CardType;
  onUpdate: (card: CardType) => void;
  onRemove: (cardId: number) => void;
  onMoveList: (cardId: number, list: string) => void;
  onMoveOrder: (cardId: number, up: boolean) => void;
  prev?: string;
  next?: string;
  up?: boolean;
  down?: boolean;
};

export const Card = ({
  card,
  onUpdate,
  onRemove,
  onMoveList,
  onMoveOrder,
  prev,
  next,
  up,
  down,
}: CardProps) => {
  const [editMode, setEditMode] = useState(false);

  if (editMode) {
    return (
      <CardEdit
        {...{
          card,
          onUpdate,
          closeEdit: () => setEditMode(false),
        }}
      />
    );
  }

  return (
    <CardView
      {...{
        card,
        onUpdate,
        onRemove,
        onMoveList,
        onMoveOrder,
        prev,
        next,
        up,
        down,
        openEdit: () => setEditMode(true),
      }}
    />
  );
};
