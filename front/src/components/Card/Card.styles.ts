import { IconChevronCompactLeft } from "@tabler/icons-react";
import styled from "styled-components";

export const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 16px;
  padding-top: 8px;
`;

export const Chevron = styled(IconChevronCompactLeft)`
  position: absolute;
  opacity: 0;
  width: 32px;
  height: 32px;
  cursor: pointer;
  color: var(--purpleDark);
  border: solid 1px transparent;

  &:hover {
    opacity: 1;
    animation: none;
    background-color: #ebecf066;
    border-radius: 4px;
    border: solid 1px #ddd;
  }
`;

export const IconPrev = styled(Chevron)`
  top: 50%;
  left: -18px;
  transform: translateY(-50%);

  &:active {
    transform: translateY(-50%) translateX(-1px);
  }
`;

export const IconNext = styled(Chevron)`
  top: 50%;
  right: -18px;
  transform: translateY(-50%) rotate(180deg);

  &:active {
    transform: translateY(-50%) rotate(180deg) translateX(-1px);
  }
`;

export const IconUp = styled(Chevron)`
  top: -18px;
  left: 50%;
  transform: translateX(-50%) rotate(90deg);

  &:active {
    transform: translateX(-50%) rotate(90deg) translateX(-1px);
  }
`;

export const IconDown = styled(Chevron)`
  bottom: -18px;
  left: 50%;
  transform: translateX(-50%) rotate(-90deg);
  &:active {
    transform: translateX(-50%) rotate(-90deg) translateX(-1px);
  }
`;

export const Title = styled.div`
  position: relative;
  font-size: 16px;
  line-height: 24px;
  font-weight: 400;
`;

export const Container = styled.div`
  position: relative;
  background-color: #fff;
  color: #222;
  padding: 16px;
  position: relative;
  border-radius: 6px;
  border: solid 1px #eee;
  box-shadow: 0 calc(0.0625rem * 1) calc(0.1875rem * 1) rgba(0, 0, 0, 0.05),
    rgba(0, 0, 0, 0.05) 0 calc(0.625rem * 1) calc(0.9375rem * 1)
      calc(-0.3125rem * 1),
    rgba(0, 0, 0, 0.04) 0 calc(0.4375rem * 1) calc(0.4375rem * 1)
      calc(-0.3125rem * 1);
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: repeat(3, auto);
  gap: 8px;

  &:hover {
    ${IconUp} {
      animation: up 0.1s ease;
      opacity: 1;
    }

    ${IconDown} {
      animation: down 0.1s ease;
      opacity: 1;
    }

    ${IconPrev} {
      animation: prev 0.1s ease;
      opacity: 1;
    }

    ${IconNext} {
      animation: next 0.1s ease;
      opacity: 1;
    }
  }

  @keyframes up {
    0% {
      opacity: 0;
      top: -12px;
    }
    100% {
      top: -18px;
      opacity: 1;
    }
  }

  @keyframes down {
    0% {
      opacity: 0;
      bottom: -12px;
    }
    100% {
      bottom: -18px;
      opacity: 1;
    }
  }

  @keyframes prev {
    0% {
      opacity: 0;
      left: -12px;
    }
    100% {
      left: -18px;
      opacity: 1;
    }
  }

  @keyframes next {
    0% {
      opacity: 0;
      right: -12px;
    }
    100% {
      right: -18px;
      opacity: 1;
    }
  }
`;

export const Order = styled.span`
  position: absolute;
  top: 0;
  right: 0;
  font-size: 12px;
  line-height: 16px;
  font-weight: 400;
  color: #999;
`;
