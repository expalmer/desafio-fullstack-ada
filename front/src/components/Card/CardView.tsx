import * as styles from "./Card.styles";

import { IconEdit, IconTrash } from "@tabler/icons-react";

import { ActionIcon } from "../ActionIcon";
import { CardType } from "../../types";
import { HtmlContent } from "../HtmlContent";

type CardProps = {
  card: CardType;
  onUpdate: (card: CardType) => void;
  onRemove: (cardId: number) => void;
  onMoveList: (cardId: number, list: string) => void;
  onMoveOrder: (cardId: number, up: boolean) => void;
  prev?: string;
  next?: string;
  up?: boolean;
  down?: boolean;
  openEdit: () => void;
};

export const CardView = ({
  card,
  onRemove,
  onMoveList,
  onMoveOrder,
  openEdit,
  prev,
  next,
  up,
  down,
}: CardProps) => {
  return (
    <styles.Container>
      <styles.Title>
        {card.titulo}
        <styles.Order>
          id:{card.id} ord:{card.ordem}
        </styles.Order>
      </styles.Title>
      <HtmlContent content={card.conteudo} />
      <styles.Actions>
        <ActionIcon>
          <IconTrash onClick={() => onRemove(card.id)} />
        </ActionIcon>
        <ActionIcon>
          <IconEdit onClick={openEdit} />
        </ActionIcon>
      </styles.Actions>
      {up && <styles.IconUp onClick={() => onMoveOrder(card.id, true)} />}
      {down && <styles.IconDown onClick={() => onMoveOrder(card.id, false)} />}
      {prev && <styles.IconPrev onClick={() => onMoveList(card.id, prev)} />}
      {next && <styles.IconNext onClick={() => onMoveList(card.id, next)} />}
    </styles.Container>
  );
};
