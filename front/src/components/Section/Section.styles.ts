import styled from "styled-components";

export const Container = styled.section`
  background-color: #fafafa;
  border-radius: 12px;
  gap: 16px;
  color: #172b4d;
  display: flex;
  justify-content: space-between;
  max-height: 100%;
  flex-direction: column;
  flex-grow: 0;
  flex-shrink: 0;
  flex-basis: 272px;
  align-self: start;
  padding: 24px;
  position: relative;
  white-space: normal;
  width: 272px;
  box-sizing: border-box;
  vertical-align: top;
  scroll-margin: 8px;
  border: solid 1px #c7bcdb;
  box-shadow: 0px 10px 15px 0px rgba(203, 179, 255, 0.6);
`;

export const Total = styled.span`
  display: flex;
  justify-content: center;
  align-items: baseline;
  height: 32px;
  font-weight: 600;
  font-size: 18px;
  font-family: Georgia, "Times New Roman", Times, serif;
  color: var(--purple);
`;
