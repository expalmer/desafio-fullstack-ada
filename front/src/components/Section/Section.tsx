import * as styles from "./Section.styles";

import { Cards, Title } from "..";

type SectionProps = {
  title: string;
  total?: number;
  icon: React.ReactNode;
  children: React.ReactNode;
};

export const Section = ({ title, total, icon, children }: SectionProps) => {
  return (
    <styles.Container>
      <Title icon={icon}>{title}</Title>
      <styles.Total>{total ?? ""}</styles.Total>
      <Cards>{children}</Cards>
    </styles.Container>
  );
};
