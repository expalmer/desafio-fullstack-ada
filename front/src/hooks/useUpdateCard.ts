import { CardType } from "../types";
import { updateCard } from "../services";
import { useMutation } from "@tanstack/react-query";

type Params = {
  onSuccess: () => void;
};

export const useUpdateCard = ({ onSuccess }: Params) => {
  return useMutation({
    mutationFn: ({ id, ...card }: CardType) => updateCard(id, card),
    mutationKey: ["update-card"],
    onSuccess,
  });
};
