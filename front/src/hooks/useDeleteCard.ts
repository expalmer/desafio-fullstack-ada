import { CardType } from "../types";
import { deleteCard } from "../services";
import { useMutation } from "@tanstack/react-query";

type Params = {
  onSuccess: () => void;
};

export const useDeleteCard = ({ onSuccess }: Params) => {
  return useMutation({
    mutationFn: (id: number) => deleteCard<CardType[]>(id),
    mutationKey: ["update-card"],
    onSuccess,
  });
};
