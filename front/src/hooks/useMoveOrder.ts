import { MoveOrderType } from "../types";
import { moveOrder } from "../services";
import { useMutation } from "@tanstack/react-query";

type Params = {
  onSuccess: () => void;
};

export const useMoveOrder = ({ onSuccess }: Params) => {
  return useMutation({
    mutationFn: ({ id, up }: MoveOrderType) => moveOrder(id, up),
    mutationKey: ["move-order"],
    onSuccess,
  });
};
