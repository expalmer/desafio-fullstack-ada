import { CardType, NewCardType } from "../types";

import { createCard } from "../services";
import { useMutation } from "@tanstack/react-query";

type Params = {
  onSuccess: () => void;
};

export const useCreateCard = ({ onSuccess }: Params) => {
  return useMutation({
    mutationFn: (card: NewCardType) => createCard<CardType, NewCardType>(card),
    mutationKey: ["create-card"],
    onSuccess,
  });
};
