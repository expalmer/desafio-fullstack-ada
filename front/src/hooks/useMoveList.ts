import { MoveListType } from "../types";
import { moveList } from "../services";
import { useMutation } from "@tanstack/react-query";

type Params = {
  onSuccess: () => void;
};

export const useMoveList = ({ onSuccess }: Params) => {
  return useMutation({
    mutationFn: ({ id, lista }: MoveListType) => moveList(id, lista),
    mutationKey: ["move-list"],
    onSuccess,
  });
};
