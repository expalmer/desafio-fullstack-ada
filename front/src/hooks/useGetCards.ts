import { CardType } from "../types";
import { getCards } from "../services";
import { isLogged } from "../utils";
import { useQuery } from "@tanstack/react-query";

export const useGetCards = () => {
  const query = useQuery({
    queryKey: ["cards"],
    queryFn: async () => {
      const { data } = await getCards<CardType[]>();
      return data;
    },
    enabled: isLogged(),
  });

  return {
    cards: query.data || [],
    isLoading: query.isLoading,
    refetch: query.refetch,
  };
};
