import { login } from "../services";
import { useEffect } from "react";
import { useMutation } from "@tanstack/react-query";

const credentials = {
  login: "letscode",
  senha: "lets@123",
};

type Token = {
  token: string;
};

export const useLogin = () => {
  const data = useMutation({
    mutationFn: () => login<Token>(credentials.login, credentials.senha),
    mutationKey: ["login"],
    onSuccess: (res) => {
      localStorage.setItem("token", res.data.token);
    },
    onError: (error) => {
      console.log(error);
    },
  });

  useEffect(() => {
    data.mutate();
  }, []);

  return;
};
