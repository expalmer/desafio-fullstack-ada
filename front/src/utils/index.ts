import "highlight.js/styles/github-dark-dimmed.css";

import { CardType, List } from "../types";

import DOMPurify from "dompurify";
import { Marked } from "marked";
import hljs from "highlight.js";
import { markedHighlight } from "marked-highlight";

const marked = new Marked(
  markedHighlight({
    async: false,
    langPrefix: "hljs language-",
    highlight(code, lang) {
      const language = hljs.getLanguage(lang) ? lang : "plaintext";
      return hljs.highlight(code, { language }).value;
    },
  })
);

export const sortTasks = (tasks: CardType[]) => {
  return tasks.sort((a, b) => a.ordem - b.ordem);
};

export const getSortedTasksByList = (tasks: CardType[], list: List) => {
  return sortTasks(tasks.filter((task) => task.lista === list));
};

export const getLastOrder = (tasks: CardType[], list: List) => {
  const tasksFiltered = getSortedTasksByList(tasks, list);
  return tasksFiltered.length
    ? tasksFiltered[tasksFiltered.length - 1].ordem
    : 0;
};

export const toView = (str: string) => {
  const html = marked.parse(str) as string;
  return DOMPurify.sanitize(html);
};

export const isLogged = () => localStorage.getItem("token") !== null;
