import axios from "axios";

export const apiClient = axios.create({
  baseURL: "http://localhost:9000",
});

apiClient.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export const login = <T>(login: string, senha: string) =>
  apiClient.post<T>("/login", { login, senha });

export const getCards = <T>() => apiClient.get<T>("/cards");

export const getCard = (id: number) => apiClient.get(`/cards/${id}`);

export const createCard = <T, B>(data: B) => apiClient.post<T>("/cards", data);

export const updateCard = <T, B>(id: number, data: B) =>
  apiClient.put<T>(`/cards/${id}`, data);

export const deleteCard = <T>(id: number) =>
  apiClient.delete<T>(`/cards/${id}`);

export const moveList = <T>(id: number, lista: string) =>
  apiClient.patch<T>(`/cards/${id}/move-list`, { lista });

export const moveOrder = <T>(id: number, up: boolean) =>
  apiClient.patch<T>(`/cards/${id}/move-order`, { up });
