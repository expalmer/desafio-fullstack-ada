import { Card, Main, NewCard, Section } from "./components";
import {
  IconCards,
  IconCode,
  IconHandGrab,
  IconHandRock,
} from "@tabler/icons-react";

import { CardType } from "./types";
import { sortTasks } from "./utils";
import { useCreateCard } from "./hooks/useCreateCard";
import { useDeleteCard } from "./hooks/useDeleteCard";
import { useGetCards } from "./hooks/useGetCards";
import { useLogin } from "./hooks/useLogin";
import { useMemo } from "react";
import { useMoveList } from "./hooks/useMoveList";
import { useMoveOrder } from "./hooks/useMoveOrder";
import { useUpdateCard } from "./hooks/useUpdateCard";

function App() {
  useLogin();

  const { cards, refetch } = useGetCards();

  const refetchCards = {
    onSuccess: () => refetch(),
  };

  const { mutate: cardCreate, isPending: isPendingCreate } =
    useCreateCard(refetchCards);

  const { mutate: cardUpdate, isPending: isPendingUpdate } =
    useUpdateCard(refetchCards);

  const { mutate: cardDelete, isPending: isPendingDelete } =
    useDeleteCard(refetchCards);

  const { mutate: moveList, isPending: isPendingMoveList } =
    useMoveList(refetchCards);

  const { mutate: moveOrder, isPending: isPendingMoveOrder } =
    useMoveOrder(refetchCards);

  const isLoading =
    isPendingCreate ||
    isPendingUpdate ||
    isPendingDelete ||
    isPendingMoveList ||
    isPendingMoveOrder;

  console.log(isLoading);

  const [todo, doing, done] = useMemo(
    () =>
      cards.reduce(
        (acc, item) => {
          if (item.lista === "todo") {
            acc[0].push(item);
          } else if (item.lista === "doing") {
            acc[1].push(item);
          } else {
            acc[2].push(item);
          }
          return acc;
        },
        [[], [], []] as CardType[][]
      ),
    [cards]
  );

  const handleMoveOrder = (cardId: number, up: boolean) => {
    moveOrder({ id: cardId, up });
  };

  const handleMoveList = (cardId: number, lista: string) => {
    moveList({ id: cardId, lista });
  };

  return (
    <Main>
      <Section title="Novo" icon={<IconCards />}>
        <NewCard onCreate={cardCreate} />
      </Section>
      <Section title="To Do" icon={<IconHandGrab />} total={todo.length}>
        {sortTasks(todo).map((card, index, array) => (
          <Card
            card={card}
            key={card.id}
            onUpdate={cardUpdate}
            onRemove={cardDelete}
            onMoveList={handleMoveList}
            onMoveOrder={handleMoveOrder}
            next="doing"
            up={!!array[index - 1]}
            down={!!array[index + 1]}
          />
        ))}
      </Section>
      <Section title="Doing" icon={<IconCode />} total={doing.length}>
        {sortTasks(doing).map((card, index, array) => (
          <Card
            card={card}
            key={card.id}
            onUpdate={cardUpdate}
            onRemove={cardDelete}
            onMoveList={handleMoveList}
            onMoveOrder={handleMoveOrder}
            prev="todo"
            next="done"
            up={!!array[index - 1]}
            down={!!array[index + 1]}
          />
        ))}
      </Section>
      <Section title="Done" icon={<IconHandRock />} total={done.length}>
        {sortTasks(done).map((card, index, array) => (
          <Card
            card={card}
            key={card.id}
            onUpdate={cardUpdate}
            onRemove={cardDelete}
            onMoveList={handleMoveList}
            onMoveOrder={handleMoveOrder}
            prev="doing"
            up={!!array[index - 1]}
            down={!!array[index + 1]}
          />
        ))}
      </Section>
    </Main>
  );
}

export default App;
